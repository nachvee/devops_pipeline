<?php
// Controller responsible for passing video subjects to the view
namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *Function return the view with the subjects
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::all();
        return view('videos.index', compact('subjects'));
    }

    /**
     * Show the video form for creating a new video.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subjects.subject_form');
    }

    /**
     * Persist a new subject.
     *
     * @return \Illuminate\Database\Eloquent\Model
     * Function stores the replies to the database and redirect user back to the video post
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'slug' => 'required'
        ]);
        $input = $request->all();
        //$input['user_id'] = auth()->user()->id;
        Subject::create($input);

        return back();
    }
}
