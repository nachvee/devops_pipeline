@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
    <div class="panel-heading">  <h1>Create Subject</h1></div>

    {!! Form::open(['action' => 'SubjectController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

    <div class="form-group">
        {{Form::label('name', 'Name')}}
        {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
    </div>

    <div class="form-group">
        {{Form::label('slug', 'Slug')}}
        {{Form::textarea('slug', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Slug'])}}
    </div>

    {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
</div>
</div>
@endsection


